fetch('https://jsonplaceholder.typicode.com/posts') 
.then((response) => response.json())
.then((data) =>{
	data.map((array)=>{
		console.log(array.title)
	})
})


fetch('https://jsonplaceholder.typicode.com/posts/1') 
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'New Post',
		body: 'Hello World!'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		description: 'New desc',
		status: 'New stat',
		dateCompleted: 'New date',
		userId: 'New Id'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		description: 'New desc',
		status: 'New stat',
		dateCompleted: 'New date',
		userId: 'New Id'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))




// var today = new Date()

// var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()
// // console.log(time)






